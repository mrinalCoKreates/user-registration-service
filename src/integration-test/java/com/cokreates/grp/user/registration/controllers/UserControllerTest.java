package com.cokreates.grp.user.registration.controllers;

import com.cokreates.grp.user.registration.helpers.dataclass.common.UserDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.request.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    // TODO: Test Headers & Exceptions

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private int totalEmployeeOffice;

//    @Before
//    public void setUp() {
//
//      objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
//      totalEmployeeOffice = repository.findAllByIsDeleted("No").size();
//    }

    @Test
    @Transactional
    public void shouldCreateEmployeeOffice() throws Exception {
        ServiceRequestDTO<UserDTO> requestDTO = new ServiceRequestDTO<>();

        UserDTO bodyDTO = new UserDTO();
        bodyDTO.setAddress("Dhaka");
        bodyDTO.setEmail("m@gmail.com");
        bodyDTO.setIsEmployee("Yes");
        bodyDTO.setMale("Male");
        bodyDTO.setMobileNo("018888888");
        bodyDTO.setNameBn("Mrinal Deb Nath");
        bodyDTO.setNameEn("Mrinal Deb Nath");
        bodyDTO.setPassword("mrinal");
        bodyDTO.setStatus("Active");
        bodyDTO.setUserType("Normal");

        requestDTO.setHeader(getHeader());
        requestDTO.setMeta(new HashMap<>());
        requestDTO.setBody(bodyDTO);

//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());

        mockMvc
                .perform(
                        post("/user/v1/create")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(requestDTO)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.body.data.[0].nameEn", is("Mrinal Deb Nath")))
                .andExpect(jsonPath("$.body.data", hasSize(1)));

//        assertEquals(totalEmployeeOffice+1, repository.findAllByIsDeleted("No").size());
    }

//    @Test
//    @Transactional
//    public void shouldThrowValidationErrorsDuringEmployeeOfficeCreation() throws Exception {
//        ServiceRequestDTO<UserDTO> requestDTO = new ServiceRequestDTO<>();
//
//        UserDTO bodyDTO = new UserDTO();
//        bodyDTO.setEmployeeOid("59846803-ff32-4667-b16c-ba341fc493a7");
//        bodyDTO.setOfficeOid("710c3d47-63a6-469d-90a6-f2c33c81f080");
//        bodyDTO.setResponsibilityType("Additional");
//        bodyDTO.setOfficeUnitOid("710c3d47-63a6-469d-90a6-f2c33c81f060");
//        bodyDTO.setOfficeUnitPostOid("710c3d47-63a6-469d-90a6-f2c33c81f090");
//        bodyDTO.setInchargeLabelEn("Test Label");
//        bodyDTO.setInchargeLabelBn("Test Label");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/create")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isBadRequest())
//                .andExpect(jsonPath("$.header.responseMessage", containsString("Validation failed. 1 error(s)")));
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//    }
//
//    @Test
//    @Transactional
//    public void shouldUpdateEmployeeOfficeByOid() throws Exception {
//        ServiceRequestDTO<UserDTO> requestDTO = new ServiceRequestDTO<>();
//
//        UserDTO bodyDTO = new UserDTO();
//        bodyDTO.setOid("033d5082-ea62-11e9-81b4-2a2ae2dbcce4");
//        bodyDTO.setEmploymentTypeOid("830bdb48-ea59-11e9-81b4-2a2ae2dbcce4");
//        bodyDTO.setEmployeeOid("a0752764-93fb-479d-a480-21dfd3a30e7c");
//        bodyDTO.setOfficeOid("710c3d47-63a6-469d-90a6-f2c33c81f080");
//        bodyDTO.setResponsibilityType("Main");
//        bodyDTO.setOfficeUnitOid("710c3d47-63a6-469d-90a6-f2c33c81f060");
//        bodyDTO.setOfficeUnitPostOid("710c3d47-63a6-469d-90a6-f2c33c81f090");
//        bodyDTO.setInchargeLabelEn("Test Label");
//        bodyDTO.setInchargeLabelBn("Test Label");
//        bodyDTO.setLastOfficeDate(Timestamp.valueOf("2019-11-11 12:30:55"));
//        bodyDTO.setStatus("Inactive");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/update")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.body.data.[0].inchargeLabelEn", is("Test Label")))
//                .andExpect(jsonPath("$.body.data", hasSize(1)));
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//    }
//
//    @Test
//    @Transactional
//    public void shouldRemoveEmployeeFromOfficeUnitPost() throws Exception {
//        ServiceRequestDTO<UserDTO> requestDTO = new ServiceRequestDTO<>();
//
//        UserDTO bodyDTO = new UserDTO();
//        bodyDTO.setOid("033d5082-ea62-11e9-81b4-2a2ae2dbcce4");
//        bodyDTO.setEmploymentTypeOid("830bdb48-ea59-11e9-81b4-2a2ae2dbcce4");
//        bodyDTO.setEmployeeOid("a0752764-93fb-479d-a480-21dfd3a30e7c");
//        bodyDTO.setOfficeOid("710c3d47-63a6-469d-90a6-f2c33c81f080");
//        bodyDTO.setResponsibilityType("Main");
//        bodyDTO.setOfficeUnitOid("710c3d47-63a6-469d-90a6-f2c33c81f060");
//        bodyDTO.setOfficeUnitPostOid("710c3d47-63a6-469d-90a6-f2c33c81f090");
//        bodyDTO.setInchargeLabelEn("Test Label");
//        bodyDTO.setInchargeLabelBn("Test Label");
//        bodyDTO.setLastOfficeDate(Timestamp.valueOf("2019-11-11 12:30:55"));
//        bodyDTO.setJoiningDate(Timestamp.valueOf("2019-11-11 12:30:55"));
//        bodyDTO.setStatus("Inactive");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/remove-employee-from-officeunitpost")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.body.data", hasSize(1)));
//
//        assertEquals(totalEmployeeOffice + 1, repository.findAllByIsDeleted("No").size());
//    }
//
//    @Test
//    @Transactional
//    public void shouldThrowValidationErrorsDuringEmployeeOfficeUpdating() throws Exception {
//        ServiceRequestDTO<UserDTO> requestDTO = new ServiceRequestDTO<>();
//
//        UserDTO bodyDTO = new UserDTO();
//        bodyDTO.setOid("033d0d66-ea62-11e9-81b4-2a2ae2dbcce4");
//        bodyDTO.setEmployeeOid("500");
//        bodyDTO.setEmploymentTypeOid("830bdb48-ea59-11e9-81b4-2a2ae2dbcce4");
//        // bodyDTO.setEmployeeOid("2");
//        bodyDTO.setOfficeOid("710c3d47-63a6-469d-90a6-f2c33c81f080");
//        bodyDTO.setResponsibilityType("Additional");
//        bodyDTO.setOfficeUnitOid("710c3d47-63a6-469d-90a6-f2c33c81f060");
//        bodyDTO.setOfficeUnitPostOid("710c3d47-63a6-469d-90a6-f2c33c81f090");
//        bodyDTO.setInchargeLabelEn("Test Label");
//        bodyDTO.setInchargeLabelBn("Test Label");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/update")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isBadRequest())
//                .andExpect(jsonPath("$.header.responseMessage", containsString("Validation failed. 2 error")));
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//    }
//
//    @Test
//    @Transactional
//    public void shouldDeleteEmployeeOfficeByOid() throws Exception {
//        ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        OidHolderRequestBodyDTO bodyDTO = new OidHolderRequestBodyDTO();
//        bodyDTO.setOid("033d2da0-ea62-11e9-81b4-2a2ae2dbcce4");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/delete")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isNoContent())
//                .andExpect(jsonPath("$.body.data", hasSize(0)));
//
//        assertEquals(totalEmployeeOffice-1, repository.findAllByIsDeleted("No").size());
//
//        assertTrue(repository.findByOidAndIsDeleted("033d2da0-ea62-11e9-81b4-2a2ae2dbcce4", "Yes").isPresent());
//    }
//
//    @Test
//    @Transactional
//    public void shouldDeleteEmployeeOfficeByOfficeUnitOid() throws Exception {
//        ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        OidHolderRequestBodyDTO bodyDTO = new OidHolderRequestBodyDTO();
//        bodyDTO.setOid("022665da-fbba-11e9-b335-362b9e155667");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/delete-by-office-unit-oid")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isNoContent())
//                .andExpect(jsonPath("$.body.data", hasSize(0)));
//
//        assertEquals(totalEmployeeOffice-3, repository.findAllByIsDeleted("No").size());
//
//        assertTrue(repository.findByOidAndIsDeleted("033d3296-ea62-11e9-81b4-2a2ae2dbcce4", "Yes").isPresent());
//    }
//
//    @Test
//    @Transactional
//    public void shouldDeleteEmployeeOfficeByOfficeUnitPostOid() throws Exception {
//        ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        OidHolderRequestBodyDTO bodyDTO = new OidHolderRequestBodyDTO();
//        bodyDTO.setOid("b3923656-fbc8-11e9-8f0b-362b9e155667");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/delete-by-office-unit-post-oid")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isNoContent())
//                .andExpect(jsonPath("$.body.data", hasSize(0)));
//
//        assertEquals(totalEmployeeOffice-1, repository.findAllByIsDeleted("No").size());
//
//        assertTrue(repository.findByOidAndIsDeleted("033d3296-ea62-11e9-81b4-2a2ae2dbcce4", "Yes").isPresent());
//    }
//
//    @Test
//    @Transactional
//    public void shouldGetActiveEmployeeOfficeByOid() throws Exception {
//        ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        OidHolderRequestBodyDTO bodyDTO = new OidHolderRequestBodyDTO();
//        bodyDTO.setOid("ca9bf274-4cc5-402e-900d-99a9f68a2bb0");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/get-active-by-employee-oid")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.body.data.[0].inchargeLabelEn", is("Deputy Secretary")))
//                .andExpect(jsonPath("$.body.data", hasSize(1)));
//
//        assertEquals(totalEmployeeOffice, repository.findAllByIsDeleted("No").size());
//
//        assertTrue(repository.findByOidAndIsDeleted("033d1018-ea62-11e9-81b4-2a2ae2dbcce4", "Yes").isPresent());
//    }
//
//    @Test
//    public void shouldReturnEmployeeOfficesByOidSet() throws Exception {
//        ServiceRequestDTO<GetListByOidSetRequestBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        GetListByOidSetRequestBodyDTO bodyDTO = new GetListByOidSetRequestBodyDTO();
//        bodyDTO.setOids(new HashSet<>(Arrays.asList("033d3296-ea62-11e9-81b4-2a2ae2dbcce4", "033d0816-ea62-11e9-81b4-2a2ae2dbcce4")));
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/get-list-by-oid-set")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.body.data.[0].oid", is("033d3296-ea62-11e9-81b4-2a2ae2dbcce4")))
//                .andExpect(jsonPath("$.body.data.[0].officeOid", is("710c3d47-63a6-469d-90a6-f2c33c81f080")))
//                .andExpect(jsonPath("$.body.data", hasSize(1)));
//    }
//
//    @Test
//    public void shouldThrowExceptionForUnavailableOidInOidSetAndStrict() throws Exception {
//        ServiceRequestDTO<GetListByOidSetRequestBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        GetListByOidSetRequestBodyDTO bodyDTO = new GetListByOidSetRequestBodyDTO();
//        bodyDTO.setOids(new HashSet<>(Arrays.asList("2", "4", "530")));
//        bodyDTO.setStrict("Yes");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/get-list-by-oid-set")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.header.responseMessage", containsString("No Employee Office Found")))
//                .andExpect(jsonPath("$.header.responseMessage", containsString("530")))
//                .andExpect(jsonPath("$.body.data", hasSize(0)));
//    }
//
//    @Test
//    public void shouldNotThrowExceptionForUnavailableOidInOidSetAndNonStrict() throws Exception {
//        ServiceRequestDTO<GetListByOidSetRequestBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        GetListByOidSetRequestBodyDTO bodyDTO = new GetListByOidSetRequestBodyDTO();
//        bodyDTO.setOids(new HashSet<>(Arrays.asList("da9ec390-f1c7-4f18-bd30-70f7c06a9b95", "530")));
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/get-list-by-oid-set")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.body.data.[0].oid", is("da9ec390-f1c7-4f18-bd30-70f7c06a9b95")))
//                .andExpect(jsonPath("$.body.data.[0].officeOid", is("9fc860ea-fbb0-11e9-8f0b-362b9e155667")))
//                .andExpect(jsonPath("$.body.data", hasSize(1)));
//    }
//
//    @Test
//    public void shouldReturnEmployeeOfficeByOid() throws Exception {
//        ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        OidHolderRequestBodyDTO bodyDTO = new OidHolderRequestBodyDTO();
//        bodyDTO.setOid("033d2da0-ea62-11e9-81b4-2a2ae2dbcce4");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/get-by-oid")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.body.data.[0].oid", is("033d2da0-ea62-11e9-81b4-2a2ae2dbcce4")))
//                .andExpect(jsonPath("$.body.data.[0].inchargeLabelEn", is("Controller")))
//                .andExpect(jsonPath("$.body.data", hasSize(1)));
//    }
//
//    @Test
//    public void shouldReturnAvailableEmployeeOffices() throws Exception {
//        ServiceRequestDTO<EmptyBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(new EmptyBodyDTO());
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/get-list")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//
//                .andExpect(jsonPath("$.body.data.[0].oid", is("033d2da0-ea62-11e9-81b4-2a2ae2dbcce4")))
//                .andExpect(jsonPath("$.body.data.[0].inchargeLabelEn", is("Controller")))
//
////                .andExpect(jsonPath("$.body.data.[0].oid", is("3")))
////                .andExpect(jsonPath("$.body.data.[0].inchargeLabelEn", is("Senior Joint Secretary")))
//
//                .andExpect(jsonPath("$.body.data", hasSize(totalEmployeeOffice)));
//    }
//
//    @Test
//    public void shouldReturnEmployeeOfficeByEmployeeOid() throws Exception {
//        ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        OidHolderRequestBodyDTO bodyDTO = new OidHolderRequestBodyDTO();
//        bodyDTO.setOid("3ea13d31-bb57-4375-b79e-a04525337422");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/get-full-info-by-employee-oid")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.body.data.[0].oid", is("3eb7a910-5c44-4d0f-9aed-a13d0547fa48")))
//
//               // .andExpect(jsonPath("$.body.data.[0].oid", is("4")))
//                // .andExpect(jsonPath("$.body.data.[0].languageOid", is("27")))
//                .andExpect(jsonPath("$.body.data", hasSize(2)));
//    }
//
//    @Test
//    public void shouldReturnEmployeeOfficeHistoryByDateAndEmployeeOid() throws Exception {
//        ServiceRequestDTO<EmployeeOfficeHistoryRequestBodyDTO> requestDTO = new ServiceRequestDTO<>();
//
//        EmployeeOfficeHistoryRequestBodyDTO bodyDTO = new EmployeeOfficeHistoryRequestBodyDTO();
//        Set<String> officeUnitPostOids = new HashSet<String>() ;
//        officeUnitPostOids.add("710c3d47-63a6-469d-90a6-f2c33c81f090");
//        officeUnitPostOids.add("b392497a-fbc8-11e9-8f0b-362b9e155667");
//        bodyDTO.setOfficeUnitPostOids(officeUnitPostOids);
//        bodyDTO.setDate("2019-11-10");
//
//        requestDTO.setHeader(getHeader());
//        requestDTO.setMeta(new HashMap<>());
//        requestDTO.setBody(bodyDTO);
//
//        mockMvc
//                .perform(
//                        post("/employee-office/v1/summary/get-employee-by-office-unit-post-oids")
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .content(objectMapper.writeValueAsString(requestDTO)))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.body.data.[0].employeeOid", is("b251d10a-adc7-4de5-ae06-f62fe1f0a0a5")))
//
//               // .andExpect(jsonPath("$.body.data.[0].oid", is("4")))
//                // .andExpect(jsonPath("$.body.data.[0].languageOid", is("27")))
//                .andExpect(jsonPath("$.body.data", hasSize(2)));
//    }

    private ServiceRequestHeaderDTO getHeader() {
        ServiceRequestHeaderDTO headerDTO = new ServiceRequestHeaderDTO();

        headerDTO.setRequestClient("grp");
        headerDTO.setRequestId("random-uuid");
        headerDTO.setRequestRetryCount(3);
        headerDTO.setRequestSource("portal");
        headerDTO.setRequestSourceService("portal");
        headerDTO.setRequestTime(new Date());
        headerDTO.setRequestTimeoutInSeconds(30);
        headerDTO.setRequestType("random");
        headerDTO.setRequestVersion("v1");

        return headerDTO;
    }
}
