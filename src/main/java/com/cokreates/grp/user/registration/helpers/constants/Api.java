package com.cokreates.grp.user.registration.helpers.constants;

import org.springframework.beans.factory.annotation.Value;

public interface Api {

    //	String BASE = "hrm/pim/";
    String BASE = "";
    
//    String CMN_ORGANOGRAM_URL = "cmn-service-organogram/cmn-service-organogram/";
//    String HRM_SERVICE_PIM_URL = "hrm-service-pim/hrm-service-pim/";
    String CMN_ORGANOGRAM_URL = "cmn-service-organogram/";
    String HRM_SERVICE_PIM_URL = "hrm-service-pim/";

	String LOCALHOST = "localhost";

	String VERSION_INFO = "v1/";

    String USER_V1_RESOURCE = BASE + "user/v1/";
    String SEARCH_V1_RESOURCE = BASE + "search/v1/";
    String BATCH_V1_RESOURCE = BASE + "batch/v1/";
    String CADRE_V1_RESOURCE = BASE + "cadre/v1/";
    String LANGUAGE_V1_RESOURCE = BASE + "language/v1/";
    String EMPLOYEE_LANGUAGE_SKILL_V1_RESOURCE = BASE + "employee-language-skill/v1/";
    String EMPLOYMENT_TYPE_V1_RESOURCE = BASE + "employment-type/v1/";
    String ADDRESS_INFO_V1_RESOURCE = BASE + "address-info/v1/";
    String GRADE_V1_RESOURCE = BASE + "grade/v1/";
    String EMPLOYEE_MASTER_V1_RESOURCE = BASE + "employee-master-info/v1/";
    String EMPLOYEE_CHILDREN_INFO_V1_RESOURCE = BASE + "employee-children-info/v1/";
    String EMPLOYEE_ADDITIONAL_PROF_QUALIFICATION_V1_RESOURCE = BASE + "employee-additional-prof-qualification/v1/";
    String EMPLOYEE_MEDICAL_INFO_V1_RESOURCE = BASE + "employee-medical-info/v1/";
    String EMPLOYEE_TRAINING_INFO_V1_RESOURCE = BASE + "employee-training-info/v1/";
    String EMPLOYEE_SPOUSE_INFO_V1_RESOURCE = BASE + "employee-spouse-info/v1/";
    String EMPLOYEE_PERSONAL_INFO_V1_RESOURCE = BASE + "employee-personal-info/v1/";
    String EMPLOYEE_OFFICE_V1_RESOURCE = BASE + "employee-office/v1/";
    String EMPLOYEE_EDUCATION_QUALIFICATION_V1_RESOURCE = BASE + "employee-education-qualification/v1/";
    String EMPLOYEE_AWARD_V1_RESOURCE = BASE + "employee-award/v1/";
    String EMPLOYEE_PUBLICATION_V1_RESOURCE = BASE + "employee-publication/v1/";
    String EMPLOYEE_OTHER_SERVICES_V1_RESOURCE = BASE + "employee-other-services/v1/";
    String EMPLOYEE_PROBATION_V1_RESOURCE = BASE + "employee-probation/v1/";
    String EMPLOYEE_CONFIRMATION_V1_RESOURCE = BASE + "employee-confirmation/v1/";
    String EMPLOYEE_DISCIPLINARY_ACTION_V1_RESOURCE = BASE + "employee-disciplinary-action/v1/";
    String EMPLOYEE_FOREIGN_TRAVEL_V1_RESOURCE = BASE + "employee-foreign-travel/v1/";

    String EMPLOYEE_V1_RESOURCE = BASE + "employee/v1/";

    String GET_BY_OID_PATH = "get-by-oid";
    String GET_LIST_PATH = "get-list";
    String GET_MINISTRY_OR_DIVISION_LIST_PATH = "get-ministry-or-division-list";
    String GET_LIST_BY_ID_SET_PATH = "get-list-by-oid-set";
    String UPDATE_COMMON_ORGANOGRAM = "update-common-organogram";
    String GET_LIST_BY_EMPLOYEE_OFFICE_ID_SET_PATH = "get-list-by-employee-office-oid-set";
    String CREATE_PATH = "create";
    String UPDATE_PATH = "update";
    String UPDATE_WITH_UNIQUE_OID = "update-with-unique-oid"; 
    String DELETE_PATH = "delete";
    String DELETE_BY_OFFICE_OID_PATH = "delete-by-office";
    String DELETE_BY_OFFICE_UNIT_OID_PATH = "delete-by-office-unit-oid";
    String DELETE_BY_OFFICE_UNIT_POST_OID_PATH = "delete-by-office-unit-post-oid";
    String GET_BY_FILTER_PATH = "get-by-filter";
    String SEARCH_BY_NAME = "search-by-name";
    String SEARCH_BY_USERID = "search-by-userid";
    String SEARCH_BY_EMPLOYEE_OID_OFFICE_OID = "search-by-employee-oid-office-oid";
    String COUNT = "count";

    String SEARCH_EMPLOYEE = "search-employees";
    String GET_BY_EMPLOYEE_OID_OFFICE_OID = "search-by-employee-oid-office-oid";
    String GET_BY_OFFICE_OFFICE_UNIT_OFFICE_UNIT_POST_OID = "get-list-by-office-officeunit-officeunitpost-oid";

    String SUMMARY_PATH = "summary/";

//  String URL = "http://localhost:8080/office-unit-post/v1/get-by-oid";
    String GET_BY_OFFICE_OID_PATH = "get-by-office-oid";
    String GET_ADMIN_BY_OFFICE_OID_PATH = "get-admin-by-office-oid";
    String GET_BY_EMPLOYEE_OID_AND_OFFICE_OID_PATH = "get-by-employee-oid-and-office-oid";

    String GET_BY_EMPLOYEE_OID_PATH = "get-by-employee-oid";
    String GET_HISTORY_BY_EMPLOYEE_OID_PATH = "get-history-by-employee-oid";
    String GET_EMPLOYEE_BY_OFFICE_UNIT_POST_OIDS = "get-employee-by-office-unit-post-oids";
    String GET_ACTIVE_BY_EMPLOYEE_OID_PATH = "get-active-by-employee-oid";
    String GET_FULL_INFO_BY_EMPLOYEE_OID_PATH = "get-full-info-by-employee-oid";

    String EMPLOYEE_REMOVE_FROM_OFFICE_UNITPOST = "remove-employee-from-officeunitpost";

}
