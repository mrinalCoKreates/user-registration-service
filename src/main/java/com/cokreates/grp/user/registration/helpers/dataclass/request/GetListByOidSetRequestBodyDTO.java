package com.cokreates.grp.user.registration.helpers.dataclass.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
public class GetListByOidSetRequestBodyDTO implements IRequestBodyDTO {

    @NotEmpty
    private Set<@NotBlank String> oids;

    private String strict;

    private String test;

}
