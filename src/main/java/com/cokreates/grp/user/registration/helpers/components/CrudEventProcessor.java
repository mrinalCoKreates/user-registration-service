package com.cokreates.grp.user.registration.helpers.components;

import com.cokreates.grp.user.registration.database.entities.BaseEntity;
import com.cokreates.grp.user.registration.helpers.constants.CrudEvent;
import org.springframework.data.util.Pair;

public interface CrudEventProcessor {
    void process(Pair<Class<? extends BaseEntity>, CrudEvent> event, String oid);
}
