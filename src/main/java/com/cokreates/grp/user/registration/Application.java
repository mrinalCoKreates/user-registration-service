package com.cokreates.grp.user.registration;


import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	@EventListener(ContextRefreshedEvent.class)
	public void contextRefreshedEvent() {

		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

	}
	
	@Bean
	public RestTemplate getRestTemplate() {		
		return new RestTemplate();
	}
	
	@Bean
	public HttpHeaders getHttpHeaders() {
		
		HttpHeaders httpHeaders = new HttpHeaders();
		
		return httpHeaders;
	}
}
