package com.cokreates.grp.user.registration.helpers.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

public class ServiceExceptionHolder {

    @Getter
    @RequiredArgsConstructor
    public static class ServiceException extends RuntimeException {
        private final int code;
        private final String message;
    }

    public static class ResourceNotFoundException extends ServiceException {
        public ResourceNotFoundException(String message) {
            super(2000, message);
        }
    }

    public static class ResourceNotFoundDuringWriteRequestException extends ServiceException {
        public ResourceNotFoundDuringWriteRequestException(String message) {
            super(4000, message);
        }
    }
    
    public static class RemoveFromOtherPostException extends ServiceException{
    	public RemoveFromOtherPostException(String message) {
    		super(5000,message);
    	}    	
    }
    
    public static class InconsistentDataException extends ServiceException{    	
    	public InconsistentDataException(String message) {    		
    		super(4000,message);
    	}    	
    }

    public static class IllegalDateFormatException extends ServiceException{
    	public IllegalDateFormatException(String message) {
    		super(HttpStatus.BAD_REQUEST.value(),message);
    	}
    }

    public static class ResourceNotGloballyReflectedException extends ServiceException {
        public ResourceNotGloballyReflectedException(String message) {
            super(1000, "At global instance: " + message);
        }
    }

    public static class ResourceNotGloballyDeletedException extends ServiceException {
        public ResourceNotGloballyDeletedException(String message) {
            super(1001, "At global instance: " + message);
        }
    }

}
