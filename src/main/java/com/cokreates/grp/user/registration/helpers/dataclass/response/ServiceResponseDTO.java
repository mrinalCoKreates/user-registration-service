package com.cokreates.grp.user.registration.helpers.dataclass.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceResponseDTO<T> {

    private ServiceResponseHeaderDTO header;

    private Map<String, Object> meta;

    private ServiceResponseBodyDTO<T> body;

}
