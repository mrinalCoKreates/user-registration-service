package com.cokreates.grp.user.registration.helpers.validators.implementations;

import com.cokreates.grp.user.registration.database.entities.BaseEntity;
import com.cokreates.grp.user.registration.helpers.components.EntityRetrieverComponent;
import com.cokreates.grp.user.registration.helpers.validators.annotations.ValidEntityOid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@RequestScope
@RequiredArgsConstructor
public class EntityOidValidityChecker implements ConstraintValidator<ValidEntityOid, String> {

    private final EntityRetrieverComponent retrieverComponent;

    private Class<? extends BaseEntity> entityClass;

    @Override
    public void initialize(ValidEntityOid annotation) {
        entityClass = annotation.value();
    }

    @Override
    public boolean isValid(String oid, ConstraintValidatorContext constraintValidatorContext) {
        return oid == null || retrieverComponent.getOptionalEntity(entityClass, oid).isPresent();
    }

}
