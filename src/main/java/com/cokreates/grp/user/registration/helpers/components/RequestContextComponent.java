package com.cokreates.grp.user.registration.helpers.components;

import com.cokreates.grp.user.registration.helpers.dataclass.request.ServiceRequestDTO;
import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope
@Data
public class RequestContextComponent {

    private ServiceRequestDTO requestDTO;

}