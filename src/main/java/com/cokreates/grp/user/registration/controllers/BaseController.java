package com.cokreates.grp.user.registration.controllers;

import com.cokreates.grp.user.registration.helpers.components.RequestContextComponent;
import com.cokreates.grp.user.registration.helpers.components.ResultRetrieverComponent;
import com.cokreates.grp.user.registration.helpers.dataclass.request.IRequestBodyDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.request.ServiceRequestDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.response.ServiceResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public abstract class BaseController {

    @Autowired
    private RequestContextComponent requestContextComponent;

    // TODO: Try to log using Interceptors if possible
    protected <R extends ServiceResponseDTO, B extends IRequestBodyDTO> R send(
            String url,
            ServiceRequestDTO<B> requestDTO,
            ResultRetrieverComponent<R, B> retriever
    ) {
        log.info("RequestSource {}", url);
        log.info("RequestBody {}", requestDTO);

        requestContextComponent.setRequestDTO(requestDTO);

        R responseDTO = retriever.retrieve(requestDTO);

        log.info("RequestSource {}", url);
        log.info("ResponseBody {}", responseDTO);

        return responseDTO;
    }
}
