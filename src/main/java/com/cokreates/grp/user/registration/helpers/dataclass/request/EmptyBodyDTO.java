package com.cokreates.grp.user.registration.helpers.dataclass.request;

import lombok.Data;

@Data
public class EmptyBodyDTO implements IRequestBodyDTO {
    private String test ;
}
