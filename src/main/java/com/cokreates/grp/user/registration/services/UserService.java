package com.cokreates.grp.user.registration.services;

import com.cokreates.grp.user.registration.database.entities.User;
import com.cokreates.grp.user.registration.helpers.components.EventManagementComponent;
import com.cokreates.grp.user.registration.helpers.components.HeaderUtilComponent;
import com.cokreates.grp.user.registration.helpers.components.IdGeneratorComponent;
import com.cokreates.grp.user.registration.helpers.dataclass.common.UserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class UserService extends BaseService<User, UserDTO> {

    public UserService(HeaderUtilComponent headerUtilComponent,
                       ModelMapper modelMapper, IdGeneratorComponent idGeneratorComponent,
                       EventManagementComponent eventManagementComponent) {
        super(headerUtilComponent, modelMapper, idGeneratorComponent, eventManagementComponent);
    }

}
