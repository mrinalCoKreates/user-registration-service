package com.cokreates.grp.user.registration.services;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.cokreates.grp.user.registration.helpers.dataclass.request.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;

import com.cokreates.grp.user.registration.database.entities.BaseEntity;
import com.cokreates.grp.user.registration.database.repositories.ServiceRepository;
import com.cokreates.grp.user.registration.helpers.components.EventManagementComponent;
import com.cokreates.grp.user.registration.helpers.components.HeaderUtilComponent;
import com.cokreates.grp.user.registration.helpers.components.IdGeneratorComponent;
import com.cokreates.grp.user.registration.helpers.constants.CrudEvent;
import com.cokreates.grp.user.registration.helpers.dataclass.response.ServiceResponseBodyDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.response.ServiceResponseDTO;
import com.cokreates.grp.user.registration.helpers.exceptions.ServiceExceptionHolder;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static com.cokreates.grp.user.registration.helpers.constants.Api.HRM_SERVICE_PIM_URL;

// TODO: Take BaseService Logic to Component with Overridden methods as interface calls
@Data
@RequiredArgsConstructor
public abstract class BaseService<E extends BaseEntity, D extends IOidHolderRequestBodyDTO> {

    private final HeaderUtilComponent headerUtilComponent;
    public final ModelMapper modelMapper;
    private final IdGeneratorComponent idGeneratorComponent;
    private final EventManagementComponent eventManagementComponent;


    @Autowired
    private HttpHeaders headers;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    HttpServletRequest request;

    <T> ServiceResponseDTO<T> generateResponse(ServiceRequestHeaderDTO header, List<T> data) {
        return new ServiceResponseDTO<>(headerUtilComponent.getResponseHeaderDTO(header),
                new HashMap<>(),
                new ServiceResponseBodyDTO<>(data));
    }

    public  ServiceResponseDTO<D> create(ServiceRequestDTO<D> requestDTO) {
        E e = convertForCreate(requestDTO.getBody());

        e.setOid(idGeneratorComponent.generateId());

        // TODO: Remove Hard-Coded IDs
        e.setCreatedBy("System");
        e.setCreatedOn(new Date());
        e.setIsDeleted("No");
        E createdEntity = e;

//        eventManagementComponent.publishCrudEvent(Pair.of(getEntityClass(), CrudEvent.CREATE), createdEntity.getOid());

        return generateResponse(requestDTO.getHeader(), Collections.singletonList(convertForRead(createdEntity)));
    }

    protected D convertForRead(E e) {
        return modelMapper.map(e, getDtoClass());
    }

    protected E convertForCreate(D d) {
        return modelMapper.map(d, getEntityClass());
    }

    @SuppressWarnings("unchecked")
    private Class<E> getEntityClass() {
        return (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @SuppressWarnings("unchecked")
    private Class<D> getDtoClass() {
        return (Class<D>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }
    
    /*<T> ServiceResponseDTO<T> generateResponse(ServiceRequestHeaderDTO header,Map<String,Object> map, List<T> data) {
        return new ServiceResponseDTO<>(headerUtilComponent.getResponseHeaderDTO(header),
                map,
                new ServiceResponseBodyDTO<>(data));
    }*/

//    public  ServiceResponseDTO<D> getByOid(ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
//        return generateResponse(requestDTO.getHeader(),
//                Collections.singletonList(convertForRead(getByOid(requestDTO.getBody().getOid()))));
//    }


//    public  ServiceResponseDTO<D> getList(ServiceRequestDTO<EmptyBodyDTO> requestDTO) {
//        return generateResponse(requestDTO.getHeader(), convertForRead(repository.findAllByIsDeleted("No")));
//    }
//
//    public  ServiceResponseDTO<D> getListByOidSet(ServiceRequestDTO<GetListByOidSetRequestBodyDTO> requestDTO) {
//        GetListByOidSetRequestBodyDTO body = requestDTO.getBody();
//        Set<String> oids = body.getOids();
//
////        List<E> eList = repository.findAllByIdAndIsDeleted(oids, "No");
//        List<E> eList = repository.findAllById(oids);
//
//        for(int i=0;i<eList.size();i++)
//        {
//            if(eList.get(i).getIsDeleted().equals("Yes"))eList.remove(i);
//        }
//
//        handleStrictness(oids, eList, body.getStrict());
//
//        return generateResponse(requestDTO.getHeader(), convertForRead(eList));
//    }
//
//    public void handleStrictness(Set<String> oids, List<E> eList, String strict) {
//        if (strict != null && strict.equals("Yes") && eList.size() < oids.size()) {
//            Map<String, Boolean> oidMap = new HashMap<>();
//            List<String> missingList = new ArrayList<>();
//
//            oids.forEach(s -> oidMap.put(s, false));
//            eList.forEach(e -> oidMap.put(e.getOid(), true));
//            oidMap.keySet().stream().filter(s -> !oidMap.get(s)).forEach(missingList::add);
//
//            throw new ServiceExceptionHolder.ResourceNotFoundException(
//                    "No "
//                            + String.join(" ", getEntityClass().getSimpleName().split("(?=\\p{Upper})"))
//                            + " Found for IDs : " + String.join(",", missingList)
//            );
//        }
//    }

//    public  ServiceResponseDTO<D> update(ServiceRequestDTO<D> requestDTO) {
//        D body = requestDTO.getBody();
//        String oid = body.getOid();
//        if (oid == null)
//            throw new ServiceExceptionHolder
//                    .ResourceNotFoundDuringWriteRequestException("No Oid Provided for " + getEntityClass().getSimpleName());
//        E e = getByOidForWrite(oid);
//        convertForUpdate(body, e);
//        e.setUpdatedBy("System");
//        e.setUpdatedOn(new Date());
//        e.setIsDeleted("No");
//        E updatedEntity = repository.save(e);
//
//        eventManagementComponent.publishCrudEvent(Pair.of(getEntityClass(), CrudEvent.UPDATE), updatedEntity.getOid());
//
//        return generateResponse(requestDTO.getHeader(), Collections.singletonList(convertForRead(updatedEntity)));
//    }
//
//    public  ServiceResponseDTO<Void> deleteByOid(ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
//        deleteEntity(getByOidForWrite(requestDTO.getBody().getOid()));
//        return generateResponse(requestDTO.getHeader(), Collections.emptyList());
//    }
//
//    protected void deleteEntity(E e) {
//        eventManagementComponent.publishCrudEvent(Pair.of(getEntityClass(), CrudEvent.DELETE), e.getOid());
//        e.setIsDeleted("Yes");
//        repository.save(e);
//
//    }
//
//    private E getByOid(@NonNull String oid) {
//        return getOptionalEntity(oid).orElseThrow(() -> new ServiceExceptionHolder.ResourceNotFoundException(
//                "No " + getEntityClass().getSimpleName() + " Found with ID: " + oid));
//    }
//
//    protected E getByOidForWrite(@NonNull String oid) {
//        return getOptionalEntity(oid)
//                .orElseThrow(() -> new ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException(
//                        "No " + getEntityClass().getSimpleName() + " Found with ID: " + oid));
//    }
//
//    private Optional<E> getOptionalEntity(@NonNull String oid) {
//        return repository.findByOidAndIsDeleted(oid, "No");
//    }
//
//    protected void convertForUpdate(D d, E e) {
//        BeanUtils.copyProperties(d, e);
//    }
//
//    List<D> convertForRead(List<E> e) {
//        return e.stream().map(this::convertForRead).collect(Collectors.toList());
//    }
//
//    public List<D> createUpdateGlobally(String url, ServiceRequestDTO<D> dto, Class dtoClass) {
//        List<D> list = null;
//        if (!global.equals("true")) {
//            try {
//                String GLOBAL_URL = GLOBAL_BASE_URL + HRM_SERVICE_PIM_URL;
//                headers.set(HttpHeaders.AUTHORIZATION, request.getHeader(HttpHeaders.AUTHORIZATION));
//                ResponseEntity<String> response = restTemplate.exchange(GLOBAL_URL + url, HttpMethod.POST, new HttpEntity(dto, headers), String.class);
//                JsonNode jsonNode = objectMapper.readTree(response.getBody());
//                JsonNode content = jsonNode.get("body").get("data");
//                list = objectMapper.readValue(
//                        content.toString(),
//                        objectMapper.getTypeFactory().constructCollectionType(
//                                List.class, dtoClass));
//            } catch (HttpStatusCodeException ex) {
//                ex.printStackTrace();
//                JsonNode jsonNode = null;
//                try {
//                    jsonNode = objectMapper.readTree(ex.getResponseBodyAsString());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                JsonNode content = jsonNode.get("body").get("data");
//                throw new ServiceExceptionHolder.ResourceNotGloballyReflectedException(
//                        content.get(0).asText());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return list;
//    }
//
//    public ServiceResponseDTO<D> searchGlobally(String url, @Valid ServiceRequestDTO<OidHolderRequestBodyDTO> dto, Class dtoClass) {
//        List<D> list = null;
//        if (!global.equals("true")) {
//            try {
//                String GLOBAL_URL = GLOBAL_BASE_URL + HRM_SERVICE_PIM_URL;
//                headers.set(HttpHeaders.AUTHORIZATION, request.getHeader(HttpHeaders.AUTHORIZATION));
//                ResponseEntity<String> response = restTemplate.exchange(GLOBAL_URL + url, HttpMethod.POST, new HttpEntity(dto, headers), String.class);
//                JsonNode jsonNode = objectMapper.readTree(response.getBody());
//                JsonNode content = jsonNode.get("body").get("data");
//                list = objectMapper.readValue(
//                        content.toString(),
//                        objectMapper.getTypeFactory().constructCollectionType(
//                                List.class, dtoClass));
//            } catch (HttpStatusCodeException ex) {
//                ex.printStackTrace();
//                JsonNode jsonNode = null;
//                try {
//                    jsonNode = objectMapper.readTree(ex.getResponseBodyAsString());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                JsonNode content = jsonNode.get("body").get("data");
//                throw new ServiceExceptionHolder.ResourceNotGloballyReflectedException(
//                        content.get(0).asText());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return generateResponse(dto.getHeader(),list);
//    }
//
//    public List<D> deleteGlobally(String url, ServiceRequestDTO<OidHolderRequestBodyDTO> dto) {
//        List<D> list = null;
//        if (!global.equals("true")) {
//            try {
//                String GLOBAL_URL = GLOBAL_BASE_URL + HRM_SERVICE_PIM_URL;
//                headers.set(HttpHeaders.AUTHORIZATION, request.getHeader(HttpHeaders.AUTHORIZATION));
//                ResponseEntity<String> response = restTemplate.exchange(GLOBAL_URL + url, HttpMethod.POST, new HttpEntity(dto, headers), String.class);
////                JsonNode jsonNode = objectMapper.readTree(response.getBody());
////                JsonNode content = jsonNode.get("body").get("data");
////                list = objectMapper.readValue(
////                        content.toString(),
////                        objectMapper.getTypeFactory().constructCollectionType(
////                                List.class, dtoClass));
//            } catch (HttpStatusCodeException ex) {
//                ex.printStackTrace();
//                JsonNode jsonNode = null;
//                try {
//                    jsonNode = objectMapper.readTree(ex.getResponseBodyAsString());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                JsonNode content = jsonNode.get("body").get("data");
//                throw new ServiceExceptionHolder.ResourceNotGloballyDeletedException(
//                        content.get(0).asText());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return list;
//    }
}
