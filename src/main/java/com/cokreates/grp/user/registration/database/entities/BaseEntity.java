package com.cokreates.grp.user.registration.database.entities;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
@Data
public class BaseEntity {

    @Id
    private String oid;
    private String isDeleted;
    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;
}
