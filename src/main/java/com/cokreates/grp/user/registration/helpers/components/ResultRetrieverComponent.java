package com.cokreates.grp.user.registration.helpers.components;

import com.cokreates.grp.user.registration.helpers.dataclass.request.IRequestBodyDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.request.ServiceRequestDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.response.ServiceResponseDTO;

public interface ResultRetrieverComponent<R extends ServiceResponseDTO, B extends IRequestBodyDTO> {

    R retrieve(ServiceRequestDTO<B> requestDTO);

}
