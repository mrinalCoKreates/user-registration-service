package com.cokreates.grp.user.registration.helpers.dataclass.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceRequestHeaderDTO {

    @NotBlank
    private String requestId;

    @NotBlank
    private String requestSource;

    @NotBlank
    private String requestSourceService;

    @NotBlank
    private String requestClient;

    @NotBlank
    private String requestType;

    @NotNull
    private Date requestTime;

    @NotBlank
    private String requestVersion;

    @NotNull
    private Integer requestTimeoutInSeconds;

    @NotNull
    @Min(0)
    private Integer requestRetryCount;

    @Min(0)
    private Integer hopCount;

    private String traceId;

}

