package com.cokreates.grp.user.registration.helpers.dataclass.common;

import com.cokreates.grp.user.registration.helpers.dataclass.request.IOidHolderRequestBodyDTO;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class UserDTO implements IOidHolderRequestBodyDTO {

    private String oid;

    @NotBlank
    private String nameEn;

    @NotBlank
    private String nameBn;

    @NotBlank
    private String mobileNo;

    @NotBlank
    private String email;

    @Pattern(regexp = "^(Male|Female|Other)$", message = "Gender must be Male,Female or Other")
    private String gender;

    private String address;

    @Pattern(regexp = "^(Yes|No)$", message = "Yes or No must be chosen for \' Is Employee? \'")
    private String isEmployee;

    @Pattern(regexp = "^(Active|Inactive)$", message = "Status must be Active or Inactive")
    private String status;

    @Pattern(regexp = "^(Super Admin|Admin|Normal)$", message = "User type must be Super Admin, Admin or Normal")
    private String userType;

    @NotBlank
    private String password;

}
