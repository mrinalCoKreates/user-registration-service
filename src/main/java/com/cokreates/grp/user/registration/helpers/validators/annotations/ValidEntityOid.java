package com.cokreates.grp.user.registration.helpers.validators.annotations;


import com.cokreates.grp.user.registration.database.entities.BaseEntity;
import com.cokreates.grp.user.registration.helpers.validators.implementations.EntityOidValidityChecker;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = EntityOidValidityChecker.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidEntityOid {
    Class<? extends BaseEntity> value();
    String message() default "invalid oid provided";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
