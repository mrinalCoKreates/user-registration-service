package com.cokreates.grp.user.registration.helpers.components;

import com.cokreates.grp.user.registration.database.entities.BaseEntity;
import com.cokreates.grp.user.registration.helpers.constants.CrudEvent;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
public class EventManagementComponent {

    private final Map<Pair<Class<? extends BaseEntity>, CrudEvent>, Set<CrudEventProcessor>> processorRegistrar;

    public EventManagementComponent() {
        processorRegistrar = new HashMap<>();
    }

    public void registerProcess(Pair<Class<? extends BaseEntity>, CrudEvent> event, CrudEventProcessor processor) {
        processorRegistrar.computeIfAbsent(event, k -> new HashSet<>());
        processorRegistrar.get(event).add(processor);
    }

    public void publishCrudEvent(Pair<Class<? extends BaseEntity>, CrudEvent> event, String oid) {
        Set<CrudEventProcessor> processors = processorRegistrar.get(event);
        if (processors != null) processors.forEach(o -> o.process(event, oid));
    }
}
