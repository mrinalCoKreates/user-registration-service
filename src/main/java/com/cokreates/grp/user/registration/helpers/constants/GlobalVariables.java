package com.cokreates.grp.user.registration.helpers.constants;

import java.util.HashMap;

public interface GlobalVariables {

    HashMap<String, String> COMMON_OFFICE_TO_URL_MAP = new HashMap<String, String>() {{
        put("Office", "office/");
        put("OfficeUnit", "office-unit/");
        put("OfficeUnitPost", "office-unit-post/");
        put("Post", "post/");
    }};

    HashMap<String, String> ENTITY_TO_TABLE_NAME = new HashMap<String, String>() {{
        put("Batch", "batch");
        put("Cadre", "cadre");
        put("Grade", "grade");
        put("Language", "language");
        put("AddressInfo", "address_info");
        put("EmploymentType", "employment_type");
        put("EmployeeMasterInfo", "employee_master_info");
        put("EmployeeOffice", "employee_office");
        put("EmployeePersonalInfo", "employee_personal_info");
        put("EmployeeChildrenInfo", "employee_children_info");
        put("EmployeeSpouseInfo", "employee_spouse_info");
        put("EmployeeEducationQualification", "employee_education_qualification");
        put("EmployeeMedicalInfo", "employee_medical_info");
        put("EmployeeLanguageSkill", "employee_language_skill");
        put("EmployeeAdditionalProfQualification", "employee_additional_prof_qualification");
        put("EmployeeOtherServices", "employee_other_services");
        put("EmployeeDisciplinaryAction", "employee_disciplinary_action");
        put("EmployeePublication", "employee_publication");
        put("EmployeeAward", "employee_award");
        put("EmployeeForeignTravel", "employee_foreign_travel");
        put("EmployeeProbation", "employee_probation");
        put("EmployeeConfirmation", "employee_confirmation");
        put("EmployeeTrainingInfo", "employee_training_info");
    }};
}
