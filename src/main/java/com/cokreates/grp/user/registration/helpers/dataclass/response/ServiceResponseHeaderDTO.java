package com.cokreates.grp.user.registration.helpers.dataclass.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceResponseHeaderDTO {

    private Date requestReceivedTime, responseTime;

    private int hopCount, responseProcessingTimeInMs;
    private String requestId, responseCode, responseMessage, responseVersion, requestSourceService, traceId;

}

