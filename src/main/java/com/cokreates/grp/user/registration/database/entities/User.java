package com.cokreates.grp.user.registration.database.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
public class User extends BaseEntity {

    private String nameEn ;
    private String nameBn;
    private String password;
    private String mobileNo;
    private String email;
    private String address;
    private String isEmployee;
    private String male;
    private String status;
    private String userType;

}
