package com.cokreates.grp.user.registration.helpers.components;

import com.cokreates.grp.user.registration.database.entities.BaseEntity;
import com.cokreates.grp.user.registration.database.repositories.ServiceRepository;
import com.cokreates.grp.user.registration.helpers.exceptions.ServiceExceptionHolder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class EntityRetrieverComponent {

    private final List<ServiceRepository> serviceRepositories;

    public <E extends BaseEntity> E getByOidForRead(Class<E> cls, String oid) {
        return getOptionalEntity(cls, oid)
                .orElseThrow(() -> new ServiceExceptionHolder.ResourceNotFoundException(
                        getMessage(oid, cls)));
    }

    public <E extends BaseEntity> E getByOidForWrite(Class<E> cls, String oid) {
        return getOptionalEntity(cls, oid)
                .orElseThrow(() -> new ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException(
                        getMessage(oid, cls)));
    }


    public <E extends BaseEntity> Optional<E> getOptionalEntity(Class<E> cls, String oid) {
        return getRepository(cls).findByOidAndIsDeleted(oid, "No");
    }

    private String getMessage(String oid, Class<?> cls) {
        return "No " + String.join(" ", cls.getSimpleName().split("(?=\\p{Upper})"))
                + " Found with ID : " + oid;
    }

    private <E extends BaseEntity> ServiceRepository<E> getRepository(Class<E> entityClass) {
        //noinspection unchecked
        return (ServiceRepository<E>) serviceRepositories.stream()
                .filter(sr -> isMatchingType(sr, entityClass))
                .findFirst()
                .orElse(null);
    }
    
    

    private boolean isMatchingType(ServiceRepository repository, Class clazz) {
        return ((ParameterizedType) ((Class) repository.getClass()
                .getGenericInterfaces()[0])
                .getGenericInterfaces()[0])
                .getActualTypeArguments()[0]
                .getTypeName().equals(clazz.getTypeName());
    }

}
