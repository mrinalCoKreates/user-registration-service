package com.cokreates.grp.user.registration.helpers.exceptions;

import com.cokreates.grp.user.registration.config.ServiceConfiguration;
import com.cokreates.grp.user.registration.helpers.components.HeaderUtilComponent;
import com.cokreates.grp.user.registration.helpers.components.RequestContextComponent;
import com.cokreates.grp.user.registration.helpers.dataclass.request.ServiceRequestDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.response.ServiceResponseBodyDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.response.ServiceResponseDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.response.ServiceResponseHeaderDTO;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.UnexpectedTypeException;
import java.util.*;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ExceptionHandlers {

    @Autowired
    private RequestContextComponent requestContextComponent;

    private final ServiceConfiguration serviceConfiguration;
    private final HeaderUtilComponent headerUtilComponent;

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(ServiceExceptionHolder.ResourceNotFoundException.class)
    public ServiceResponseDTO handleUserNotFoundException(final ServiceExceptionHolder.ResourceNotFoundException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(ServiceExceptionHolder.RemoveFromOtherPostException.class)
    public ServiceResponseDTO handleMorePostException(final ServiceExceptionHolder.RemoveFromOtherPostException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException.class)
    public ServiceResponseDTO handleUserNotFoundException(final ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ServiceResponseDTO handleUnsupportedMediaType(final HttpMediaTypeNotSupportedException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getLocalizedMessage()));
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ServiceResponseDTO handleValidationError(final HttpMessageNotReadableException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse("Could not read the request body"));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ServiceResponseDTO handleInvalidRequestParam(final MethodArgumentTypeMismatchException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse("Invalid Request Parameters"));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(UnexpectedTypeException.class)
    public ServiceResponseDTO handleInvalidRequestParam(final UnexpectedTypeException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse("Invalid Request Parameters"));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ServiceResponseDTO handleValidationError(final MethodArgumentNotValidException ex) {
        Errors validationErrors = ex.getBindingResult();
        String message = "Validation failed. " + validationErrors.getErrorCount() + " error(s)";

        List<String> errors = new ArrayList<>();

        for (FieldError fieldError : validationErrors.getFieldErrors())
            errors.add(Objects.requireNonNull(fieldError).getField() + " " + fieldError.getDefaultMessage());

        for (ObjectError objectError : validationErrors.getGlobalErrors())
            errors.add(objectError.getDefaultMessage());

        return getProcessedApiErrorResponse(new ApiErrorResponse(message, errors));
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public ServiceResponseDTO handleNoHandlerFoundError(final NoHandlerFoundException e) {
        return getProcessedApiErrorResponse(new ApiErrorResponse("Invalid URL"));
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ServiceResponseDTO handleThrowable(final Exception ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse(
                "Internal server error while processing request.",
                ex.getMessage()
        ));
    }

    private ServiceResponseDTO<String> getProcessedApiErrorResponse(ApiErrorResponse apiErrorResponse) {
        log.error(apiErrorResponse.toString());
        ServiceRequestDTO requestDTO = requestContextComponent.getRequestDTO();

        ServiceResponseHeaderDTO header = requestDTO != null ?
                headerUtilComponent.getResponseHeaderDTO(requestDTO.getHeader()) :
                headerUtilComponent.getResponseHeaderDTO(apiErrorResponse);

        header.setResponseMessage(apiErrorResponse.getMessage());

        ServiceResponseBodyDTO<String> body = new ServiceResponseBodyDTO<>();
        body.setData(apiErrorResponse.getErrors());

        return new ServiceResponseDTO<>(header, new HashMap<>(), body);
    }

    // TODO: Conform to API Response Pattern Especially for Error Response
    @Data
    @Setter(AccessLevel.PRIVATE)
    public class ApiErrorResponse {

        private ServiceResponseHeaderDTO header;
        private final String code;
        private final String message;
        private final List<String> errors;

        private ApiErrorResponse(String message, List<String> errors, ServiceExceptionHolder.ServiceException e) {
            this.code = serviceConfiguration.getShortCode() + e.getCode();
            this.message = message;
            this.errors = errors;
        }

        private ApiErrorResponse(String message, ServiceExceptionHolder.ServiceException e) {
            this(message, new ArrayList<>(), e);
        }

        private ApiErrorResponse(String message, List<String> errors) {
            this(message, errors, new ServiceExceptionHolder.ServiceException(1000, message));
        }

        private ApiErrorResponse(String message) {
            this(message, Collections.singletonList(message));
        }

        private ApiErrorResponse(String message, String errorMessage) {
            this(message, Collections.singletonList(errorMessage));
        }
    }
}