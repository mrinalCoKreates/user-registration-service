package com.cokreates.grp.user.registration.helpers.dataclass.response;

import lombok.Data;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

@Data
public class ServiceResponseBodyDTO<T> {

    private List<T> data;

    public ServiceResponseBodyDTO(@NonNull List<T> data) {
        setData(data);
    }

    public ServiceResponseBodyDTO() {
        this(new ArrayList<>());
    }

    public void setData(@NonNull List<T> data) {
        this.data = data;
    }
}
