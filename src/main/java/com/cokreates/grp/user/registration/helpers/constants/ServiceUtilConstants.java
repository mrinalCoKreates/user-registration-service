package com.cokreates.grp.user.registration.helpers.constants;

public interface ServiceUtilConstants {
    String BOOLEAN_REGEX_PATTERN = "^(Yes|No)$";
    String NOT_ASSIGNED = "Not Assigned";

}
