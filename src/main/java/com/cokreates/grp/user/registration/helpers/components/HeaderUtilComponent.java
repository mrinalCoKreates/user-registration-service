package com.cokreates.grp.user.registration.helpers.components;

import com.cokreates.grp.user.registration.config.ServiceConfiguration;
import com.cokreates.grp.user.registration.helpers.dataclass.request.ServiceRequestHeaderDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.response.ServiceResponseHeaderDTO;
import com.cokreates.grp.user.registration.helpers.exceptions.ExceptionHandlers;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class HeaderUtilComponent {

    private final ServiceConfiguration serviceConfiguration;

    public ServiceResponseHeaderDTO getResponseHeaderDTO(ServiceRequestHeaderDTO requestHeaderDTO) {
        ServiceResponseHeaderDTO responseHeaderDTO = new ServiceResponseHeaderDTO();

        responseHeaderDTO.setRequestReceivedTime(requestHeaderDTO.getRequestTime());
        responseHeaderDTO.setResponseTime(new Date());
        responseHeaderDTO.setHopCount(
                requestHeaderDTO.getHopCount() == null ? 1 : requestHeaderDTO.getHopCount() + 1
        );
        /*responseHeaderDTO.setResponseProcessingTimeInMs(Math.toIntExact(
                responseHeaderDTO.getResponseTime().getTime() - requestHeaderDTO.getRequestTime().getTime()
        ));*/
        responseHeaderDTO.setRequestId(requestHeaderDTO.getRequestId());
        responseHeaderDTO.setResponseCode(String.valueOf(HttpStatus.OK.value()));
        responseHeaderDTO.setResponseMessage("Successfully performed");
        responseHeaderDTO.setResponseVersion(serviceConfiguration.getVersion());
        responseHeaderDTO.setRequestSourceService(requestHeaderDTO.getRequestSourceService());
        responseHeaderDTO.setTraceId(
                (requestHeaderDTO.getTraceId() == null || requestHeaderDTO.getTraceId().isEmpty()) ?
                        UUID.randomUUID().toString() : requestHeaderDTO.getTraceId());
        return responseHeaderDTO;
    }

    public ServiceResponseHeaderDTO getResponseHeaderDTO(ExceptionHandlers.ApiErrorResponse apiErrorResponse) {
        ServiceResponseHeaderDTO responseHeaderDTO = new ServiceResponseHeaderDTO();

        responseHeaderDTO.setRequestReceivedTime(new Date());
        responseHeaderDTO.setResponseTime(new Date());
        responseHeaderDTO.setHopCount(1);
        responseHeaderDTO.setResponseProcessingTimeInMs(Math.toIntExact(
                responseHeaderDTO.getResponseTime().getTime() - responseHeaderDTO.getRequestReceivedTime().getTime()
        ));
        responseHeaderDTO.setRequestId(UUID.randomUUID().toString());
        responseHeaderDTO.setResponseCode(apiErrorResponse.getCode());
        responseHeaderDTO.setResponseMessage(apiErrorResponse.getMessage());
        responseHeaderDTO.setResponseVersion(serviceConfiguration.getVersion());
        responseHeaderDTO.setTraceId(UUID.randomUUID().toString());
        return responseHeaderDTO;
    }
    
    public ServiceRequestHeaderDTO getRequestHeaderDTO() {
    	ServiceRequestHeaderDTO requestHeaderDTO = new ServiceRequestHeaderDTO();
        requestHeaderDTO.setRequestId( "random-uuid");
        requestHeaderDTO.setRequestSource("portal");
        requestHeaderDTO.setRequestSourceService("portal");
        requestHeaderDTO.setRequestClient("grp");
        requestHeaderDTO.setRequestType("random");
        requestHeaderDTO.setRequestTime(new Date());
        requestHeaderDTO.setRequestVersion("v1");
        requestHeaderDTO.setRequestTimeoutInSeconds(30);
        requestHeaderDTO.setRequestRetryCount(3);    	
    	return requestHeaderDTO;
    	
    }


}
