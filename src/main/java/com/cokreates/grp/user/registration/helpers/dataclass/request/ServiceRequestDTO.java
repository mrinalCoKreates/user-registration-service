package com.cokreates.grp.user.registration.helpers.dataclass.request;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
public class ServiceRequestDTO<T extends IRequestBodyDTO> {

    @NotNull
    @Valid
    private ServiceRequestHeaderDTO header;

    @NotNull
    @Valid
    private Map<String, Object> meta;

    @NotNull
    @Valid
    private T body;

}
