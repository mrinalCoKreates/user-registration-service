package com.cokreates.grp.user.registration.helpers.constants;

public enum CrudEvent {
    CREATE,
    UPDATE,
    DELETE
}
