package com.cokreates.grp.user.registration.helpers.dataclass.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MultipleOidHolderRequestBodyDTO implements IMultipleOidHolderRequestBodyDTO {

    private String employeeMasterInfoOid;
    private String officeOid;
}
