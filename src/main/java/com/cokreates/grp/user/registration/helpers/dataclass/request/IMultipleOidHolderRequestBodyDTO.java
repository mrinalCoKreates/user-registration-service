package com.cokreates.grp.user.registration.helpers.dataclass.request;

public interface IMultipleOidHolderRequestBodyDTO extends IRequestBodyDTO {

    String getEmployeeMasterInfoOid();

    void setEmployeeMasterInfoOid(String employeeMasterInfoOid);

    String getOfficeOid();

    void setOfficeOid(String officeOid);

}
