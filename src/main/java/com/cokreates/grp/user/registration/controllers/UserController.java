package com.cokreates.grp.user.registration.controllers;

import com.cokreates.grp.user.registration.helpers.components.ResultRetrieverComponent;
import com.cokreates.grp.user.registration.helpers.constants.Api;
import com.cokreates.grp.user.registration.helpers.dataclass.common.UserDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.request.EmptyBodyDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.request.GetListByOidSetRequestBodyDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.request.OidHolderRequestBodyDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.request.ServiceRequestDTO;
import com.cokreates.grp.user.registration.helpers.dataclass.response.ServiceResponseDTO;
import com.cokreates.grp.user.registration.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(Api.USER_V1_RESOURCE)
public class UserController extends BaseController {

    private final UserService service;

    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    @PostMapping(Api.CREATE_PATH)
    public ServiceResponseDTO<UserDTO>
    createBatch(@Valid @RequestBody ServiceRequestDTO<UserDTO> requestDTO) {
        return send(Api.USER_V1_RESOURCE + Api.CREATE_PATH, requestDTO, new ResultRetrieverComponent<ServiceResponseDTO<UserDTO>, UserDTO>() {
            @Override
            public ServiceResponseDTO<UserDTO> retrieve(ServiceRequestDTO<UserDTO> requestDTO1) {
                return service.create(requestDTO1);
            }
        });
    }

//    @PostMapping(Api.GET_BY_OID_PATH)
//    public ServiceResponseDTO<UserDTO>
//    getBatchByOid(@Valid @RequestBody ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
//        return send(Api.USER_V1_RESOURCE + Api.GET_BY_OID_PATH, requestDTO, service::getByOid);
//    }
//
//    @PostMapping(Api.GET_LIST_PATH)
//    public ServiceResponseDTO<UserDTO>
//    getBatchList(@Valid @RequestBody ServiceRequestDTO<EmptyBodyDTO> requestDTO) {
//        return send(Api.USER_V1_RESOURCE + Api.GET_LIST_PATH, requestDTO, new ResultRetrieverComponent<ServiceResponseDTO<UserDTO>, EmptyBodyDTO>() {
//            @Override
//            public ServiceResponseDTO<UserDTO> retrieve(ServiceRequestDTO<EmptyBodyDTO> requestDTO1) {
//                return service.getList(requestDTO1);
//            }
//        });
//    }
//
//    @PostMapping(Api.GET_LIST_BY_ID_SET_PATH)
//    public ServiceResponseDTO<UserDTO>
//    getBatchListByIdSet(@Valid @RequestBody ServiceRequestDTO<GetListByOidSetRequestBodyDTO> requestDTO) {
//        return send(Api.USER_V1_RESOURCE + Api.GET_LIST_BY_ID_SET_PATH, requestDTO, service::getListByOidSet);
//    }
//
//    @Transactional
//    @PostMapping(Api.UPDATE_PATH)
//    public ServiceResponseDTO<UserDTO>
//    updateBatch(@Valid @RequestBody ServiceRequestDTO<UserDTO> requestDTO) {
//        service.createUpdateGlobally(Api.USER_V1_RESOURCE + Api.UPDATE_PATH, requestDTO, UserDTO.class);
//        return send(Api.USER_V1_RESOURCE + Api.UPDATE_PATH, requestDTO, service::update);
//    }
//
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    @Transactional
//    @PostMapping(Api.DELETE_PATH)
//    public ServiceResponseDTO<Void>
//    deleteBatch(@Valid @RequestBody ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
//        service.deleteGlobally(Api.USER_V1_RESOURCE + Api.DELETE_PATH, requestDTO);
//        return send(Api.USER_V1_RESOURCE + Api.DELETE_PATH, requestDTO, service::deleteByOid);
//    }

}
